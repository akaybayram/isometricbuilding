#include "game.hpp"
#include "config.hpp"

Game::Game(){
    std::cout << "Openning Window..." << std::endl;
    InitWindow(WIDTH, HEIGHT, TITLE);
    this->init();
    this->main_loop();
}

Game::~Game(){
    CloseWindow();
    std::cout << "[ INFO ] Window Closed!" << std::endl;
}
void Game::init(){
    return;
}

void Game::update(){
    return;
}

void Game::draw(){
    ClearBackground(RAYWHITE);
    return;

}

void Game::main_loop(){
    while(!WindowShouldClose()){
        this->update();
        BeginDrawing();
        this->draw();
        EndDrawing();
    }
    return;
}
