#ifndef GAME_HPP_
#define GAME_HPP_

#include <iostream>
#include <raylib.h>

class Game{

public:
    Game();
    ~Game();

    void init();
    void update();
    void draw();
    void main_loop();

};


#endif // GAME_HPP_
