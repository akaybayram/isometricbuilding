CC=g++
ifdef RELEASE
	OUTFILE=game
	CPPFLAGS=-Wall -Wextra -O3 -std=c++17
else
	OUTFILE=game_debug
	CPPFLAGS=-Wall -Wextra -ggdb -std=c++17
endif
LFLAGS=-lraylib
CPPFILES=$(wildcard src/*.cpp)
CPPFILENAMES=$(patsubst %.cpp, %, $(CPPFILES))
OBJS=$(patsubst %.cpp, %.o, $(CPPFILES))

all: $(OUTFILE)

%.o : %.c
	$(CC) -c $(CPPFLAGS) $(LFLAGS) $< -o $@

$(OUTFILE): $(OBJS)
	$(CC) -o $@ $(OBJS) $(CPPFLAGSDEBUG) $(LFLAGS)

clean:
	rm $(OBJS) $(OUTFILE)
